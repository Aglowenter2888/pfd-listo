<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{     
    //Metodos PDF CapturaExp 
    public function pdfForm()
    {
        return view('expedientes.create');
    }
 
    public function pdfDownload(Request $request){
 
       request()->validate([
        'Titulo' => 'required',
        'Asunto' => 'required',
        'Date' => 'required',
        'Descripcion' => 'required',
        'Docente' => 'required',
        'Area' => 'required'
        ]);
      
        $data = [
            'Titulo' => $request->Titulo,
            'Asunto' => $request->Asunto,
            'Date' => $request->Date,
            'Descripcion' => $request->Descripcion,
            'Docente' => $request->Docente,
            'Area' => $request->Area
        ];

        $pdf = PDF::loadView('pdf_download', $data);
   
        return $pdf->stream('informe.pdf');
    }

    //PDF Gestion Academica
    public function pdfFormMaterial()
    {
        return view('users.Material');
    }

    public function pdfDownload_Material(Request $request){
        request()->validate([
            'autor' => 'required',
            'titulo' => 'required',
            'descripcion' => 'required',
            'pais' => 'required',
            'editorial' => 'required',
            'proposito' => 'required',
            'miembros' => 'required',
            'lgacs' => 'required',
            'cv' => 'required',
            'date' => 'required',
            'area' => 'required'
        ]);
          
        $data = [
            'autor' => $request->autor,
            'titulo' => $request->titulo,
            'descripcion' => $request->descripcion,
            'pais' => $request->pais,
            'editorial' => $request->editorial,
            'proposito' => $request->proposito,
            'miembros' => $request->miembros,
            'lgacs' => $request->lgacs,
            'cv' => $request->cv,
            'date' => $request->date,
            'area' => $request->area
        ];
        
        $pdf = PDF::loadView('pdf_download_Material', $data);
       
        return $pdf->stream('informeGestion.pdf');
    }

    //PDF Productos Desarrollados
    public function pdfFormProductos()
    {
        return view('users.Productos_desarrollados');
    }

    public function pdfDownload_Productos(Request $request){
        request()->validate([
            'NombreArticulo' => 'required',
            'NombreInforme' => 'required',
            'Proyecto' => 'required',
            'Patente' => 'required',
            'Libro' => 'required',
            'Capitulo' => 'required',
            'Date' => 'required',
            'Area' => 'required'
        ]);

        $data = [
            'NombreArticulo' => $request->NombreArticulo,
            'NombreInforme' => $request->NombreInforme,
            'Proyecto' => $request->Proyecto,
            'Patente' => $request->Patente,
            'Libro' => $request->Libro,
            'Capitulo' => $request->Capitulo,
            'Date' => $request->Date,
            'Area' => $request->Area
        ];
        
        $pdf = PDF::loadView('pdf_download_ProductosD', $data);

        return $pdf->stream('informeGestion.pdf');
    }
}